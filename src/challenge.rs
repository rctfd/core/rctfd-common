use crate::User;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::error::Error;
use std::fmt::Debug;

/// ID which represents a challenge.
pub type ChallengeID = u64;

/// Challenge type which will be commonly used between each MVC component and plugin.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Challenge {
    id: ChallengeID,
    name: String,
}

impl Challenge {
    /// Fetches the id for this challenge.
    #[must_use]
    pub fn id(&self) -> ChallengeID {
        self.id
    }

    /// Fetches the name of this challenge.
    #[must_use]
    pub fn name(&self) -> &String {
        &self.name
    }

    /// Generates a Challenge instance with the specified values. This will likely only need to be
    /// used by database types.
    pub fn new(id: u64, name: &str) -> Self {
        Challenge {
            id,
            name: name.to_string(),
        }
    }
}

/// Database which controls the mutability of challenges. This should be implemented as the bridge
/// between view, controller, and model such that the modifications to the challenge at any location
/// are reflected back to the model.
#[async_trait]
pub trait ChallengeDatabase: Send + Sync {
    /// The error type employed by this challenge database
    type DBError: Error + Clone;
    /// Creates a new user in the database.
    async fn new_challenge(&mut self, name: &str) -> Result<Challenge, Self::DBError>;

    /// Gets a user by ID.
    async fn challenge(&self, id: ChallengeID) -> Result<Challenge, Self::DBError>;
}

/// Solver determines whether or not a user has successfully solved challenges with a provided flag.
#[async_trait]
pub trait Solver: Debug + Send + Sync {
    /// Identifies challenges solved by a user using a given flag.
    async fn solve(&self, user: &User, flag: &[u8]) -> Vec<ChallengeID>;
}

impl PartialEq for Challenge {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl PartialOrd for Challenge {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}
