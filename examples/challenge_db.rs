//! Example which demonstrates the creation and usage of ChallengeDatabase implementations.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

pub use example::{MyChallengeDB, MyChallengeDBError};
use rctfd_common::ChallengeDatabase;
use std::error::Error;

mod example {
    use async_trait::async_trait;
    use rctfd_common::{Challenge, ChallengeDatabase, ChallengeID};
    use std::error::Error;
    use std::fmt::{Display, Formatter};

    /// A personal implementation of the challenge database which stores values locally in the
    /// struct.
    #[derive(Debug)]
    pub struct MyChallengeDB {
        id: ChallengeID,
        challenges: Vec<Challenge>,
    }

    impl MyChallengeDB {
        /// Creates a new MyChallengeDB instance.
        pub fn new() -> Self {
            MyChallengeDB {
                id: 0,
                challenges: Vec::new(),
            }
        }
        /// Gets all of the challenges in this MyUserDB instance. Yay, bonus functionality!
        pub async fn challenges(&self) -> Result<Vec<Challenge>, MyChallengeDBError> {
            Ok(self.challenges.clone())
        }
    }

    impl Default for MyChallengeDB {
        fn default() -> Self {
            MyChallengeDB::new()
        }
    }

    /// Error which represents some failure in the database.
    #[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
    pub enum MyChallengeDBError {
        /// Error returned by the database in the scenario that the challenge already existed during
        /// the attempt to create a new challenge.
        ChallengeAlreadyExists,
        /// Error returned by the database in the scenario that a challenge was queried for or
        /// updated but did not exist.
        NoSuchChallenge,
    }

    #[cfg_attr(tarpaulin, skip)]
    impl Error for MyChallengeDBError {}

    impl Display for MyChallengeDBError {
        #[cfg_attr(tarpaulin, skip)]
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
            match self {
                MyChallengeDBError::ChallengeAlreadyExists => {
                    write!(f, "Challenge already exists!")
                }
                MyChallengeDBError::NoSuchChallenge => write!(f, "Challenge doesn't exist!"),
            }
        }
    }

    #[async_trait]
    impl ChallengeDatabase for MyChallengeDB {
        type DBError = MyChallengeDBError;

        async fn new_challenge(&mut self, name: &str) -> Result<Challenge, MyChallengeDBError> {
            if self
                .challenges
                .iter()
                .any(|challenge| challenge.name() == name)
            {
                Err(MyChallengeDBError::ChallengeAlreadyExists)
            } else {
                let challenge = Challenge::new(self.id, name);
                self.id += 1;
                self.challenges.push(challenge.clone());
                Ok(challenge)
            }
        }

        async fn challenge(&self, id: u64) -> Result<Challenge, MyChallengeDBError> {
            match self.challenges.get(id as usize).cloned() {
                None => Err(MyChallengeDBError::NoSuchChallenge),
                Some(challenge) => Ok(challenge),
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut db = MyChallengeDB::new();

    // ensure that the correct errors are returned
    assert_eq!(
        MyChallengeDBError::NoSuchChallenge,
        db.challenge(0).await.unwrap_err()
    );

    // lmao checks
    let challenge = db.new_challenge("lmao").await?;

    // basic challenge data testing
    assert_eq!(0, challenge.id());
    assert_eq!("lmao", challenge.name());
    assert_eq!(challenge, db.challenge(0).await?);
    assert_eq!(vec![challenge], db.challenges().await?);

    // no duplicates
    assert_eq!(
        MyChallengeDBError::ChallengeAlreadyExists,
        db.new_challenge("lmao").await.unwrap_err()
    );

    Ok(())
}
