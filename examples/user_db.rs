//! Example which demonstrates the creation and usage of UserDatabase implementations.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

pub use example::{MyUserDB, MyUserDBError, StaticSolver};
use rctfd_common::UserDatabase;
use std::error::Error;

mod example {
    use async_trait::async_trait;
    use rctfd_common::{ChallengeID, Solver, User, UserDatabase, UserID};
    use serde::export::Formatter;
    use std::error::Error;
    use std::fmt::Display;

    /// A static solver for which there can be more than one flag per challenge.
    #[derive(Debug)]
    pub struct StaticSolver {
        challenges: Vec<(Box<[u8]>, ChallengeID)>,
    }

    impl StaticSolver {
        /// Creates a new static solver.
        pub fn new() -> Self {
            StaticSolver {
                challenges: Vec::new(),
            }
        }

        /// Adds a solution to the static solver.
        pub fn add_solution(&mut self, flag: Box<[u8]>, id: ChallengeID) {
            self.challenges.push((flag, id));
        }
    }

    impl Default for StaticSolver {
        #[cfg_attr(tarpaulin, skip)]
        fn default() -> Self {
            StaticSolver::new()
        }
    }

    #[async_trait]
    impl Solver for StaticSolver {
        async fn solve(&self, user: &User, flag: &[u8]) -> Vec<u64> {
            self.challenges
                .iter()
                .filter(|(f, id)| f.as_ref() == flag && !user.solves().contains(id))
                .map(|(_, id)| id)
                .cloned()
                .collect()
        }
    }

    /// A personal implementation of the user database which stores values locally in the struct.
    #[derive(Debug)]
    pub struct MyUserDB {
        id: UserID,
        users: Vec<User>,
    }

    impl MyUserDB {
        /// Creates a new MyUserDB instance.
        pub fn new() -> Self {
            MyUserDB {
                id: 0,
                users: Vec::new(),
            }
        }
        /// Gets all of the users in this MyUserDB instance. Yay, bonus functionality!
        pub async fn users(&self) -> Result<Vec<User>, MyUserDBError> {
            Ok(self.users.clone())
        }
    }

    impl Default for MyUserDB {
        fn default() -> Self {
            MyUserDB::new()
        }
    }

    /// Error which represents some failure in the database.
    #[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
    pub enum MyUserDBError {
        /// Error returned by the database in the scenario that the user already existed during the
        /// attempt to create a new user.
        UserAlreadyExists,
        /// Error returned by the database in the scenario that a user was queried for or updated
        /// but did not exist.
        NoSuchUser,
    }

    #[cfg_attr(tarpaulin, skip)]
    impl Error for MyUserDBError {}

    impl Display for MyUserDBError {
        #[cfg_attr(tarpaulin, skip)]
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
            match self {
                MyUserDBError::UserAlreadyExists => write!(f, "User already exists!"),
                MyUserDBError::NoSuchUser => write!(f, "User doesn't exist!"),
            }
        }
    }

    #[async_trait]
    impl UserDatabase for MyUserDB {
        type DBError = MyUserDBError;

        async fn new_user(&mut self, name: &str) -> Result<User, MyUserDBError> {
            if self.users.iter().any(|user| user.name() == name) {
                Err(MyUserDBError::UserAlreadyExists)
            } else {
                let user = User::new(self.id, name, Vec::new());
                self.id += 1;
                self.users.push(user.clone());
                Ok(user)
            }
        }

        async fn user(&self, id: u64) -> Result<User, MyUserDBError> {
            match self.users.get(id as usize).cloned() {
                None => Err(MyUserDBError::NoSuchUser),
                Some(user) => Ok(user),
            }
        }

        async fn solve(
            &mut self,
            id: UserID,
            solver: &dyn Solver,
            flag: &[u8],
        ) -> Result<(User, Vec<u64>), MyUserDBError> {
            match self.users.get_mut(id as usize) {
                None => Err(MyUserDBError::NoSuchUser),
                Some(user) => {
                    let res = solver.solve(user, flag).await;
                    let mut solves: Vec<u64> = user.solves().clone();
                    for solve in &res {
                        solves.push(*solve);
                    }
                    *user = User::new(user.id(), user.name(), solves);
                    Ok((user.clone(), res))
                }
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // create a solver with a single challenge
    let mut solver = StaticSolver::new();
    solver.add_solution(b"correct".to_vec().into_boxed_slice(), 0);

    // initialise the database
    let mut db = MyUserDB::new();

    // ensure that the correct errors are returned
    assert_eq!(MyUserDBError::NoSuchUser, db.user(0).await.unwrap_err());
    assert_eq!(
        MyUserDBError::NoSuchUser,
        db.solve(0, &solver, b"correct").await.unwrap_err()
    );

    // billy checks
    let user = db.new_user("billy").await?;

    // basic user data testing
    assert_eq!(0, user.id());
    assert_eq!("billy", user.name());
    assert_eq!(0, user.solves().len());
    assert_eq!(user, db.user(0).await?);
    assert_eq!(vec![user.clone()], db.users().await?);

    // no duplicates
    assert_eq!(
        MyUserDBError::UserAlreadyExists,
        db.new_user("billy").await.unwrap_err()
    );

    // attempt solve -- incorrect
    let res = db.solve(0, &solver, b"incorrect").await?;
    assert_eq!(user, res.0);
    assert_eq!(0, res.1.len());

    // attempt solve -- correct
    let res = db.solve(0, &solver, b"correct").await?;
    assert_eq!(user, res.0);
    assert_eq!(1, res.1.len());
    assert_eq!(Some(&0), res.0.solves().first());
    assert_eq!(Some(&0), res.1.first());

    Ok(())
}
