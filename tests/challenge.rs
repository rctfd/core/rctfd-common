//! Test the basic components of the challenge type.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

use rand::random;
use rctfd_common::Challenge;

#[test]
fn test_id() {
    let id = random();
    let user = Challenge::new(id, "");
    assert_eq!(id, user.id());
}

#[test]
fn test_name() {
    let challenge = Challenge::new(0, "lmao");
    assert_eq!("lmao", challenge.name());
}

#[test]
fn test_eq() {
    let id = random();
    let chal1 = Challenge::new(id, "");
    let chal2 = Challenge::new(id, "");
    assert_eq!(id, chal1.id());
    assert_eq!(id, chal2.id());
    assert_eq!(chal1, chal2);
}

#[test]
fn test_ord() {
    let id = random::<u32>() as u64; // don't overflow, please
    let user1 = Challenge::new(id, "");
    let user2 = Challenge::new(id + 1, "");
    assert_eq!(id, user1.id());
    assert_eq!(id + 1, user2.id());
    assert!(user1 < user2);
}
